<?php declare(strict_types=1);

namespace App\Api\Controllers;

use Arrilot\Api\Skeleton\BaseController;

abstract class Controller extends BaseController
{
}
